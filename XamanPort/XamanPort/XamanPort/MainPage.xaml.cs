﻿using System;
using Xamarin.Forms;
using FFImageLoading.Svg.Forms;

namespace XamanPort
{
    public partial class MainPage : ContentPage
    {
        
        private WebConnector m_restDriver;

        public MainPage()
        {
            InitializeComponent();
            m_restDriver = new WebConnector();
        }
            

        private void butao_Clicked(object sender, EventArgs e)
        {
            refresh();
        }

        private void xa_textinput_Completed(object sender, EventArgs e)
        {
            refresh();
        }

        private void refresh()
        {
            if (m_restDriver.requestData(xa_textinput.Text)) {
                xa_pais.Text = "Nome: " + m_restDriver.m_pais.Name;
                xa_capital.Text = "Capital: " + m_restDriver.m_pais.Capital;
                xa_regiao.Text = "Continente: " + m_restDriver.m_pais.Region;
                xa_populacao.Text = "População: " + m_restDriver.m_pais.Population;
                xa_bandeira.Text = "Bandeira: " + m_restDriver.m_pais.Flag;

                xa_flag.Source = SvgImageSource.FromUri(new Uri(m_restDriver.m_pais.Flag));
                xa_flag.ReloadImage();
                DisplayAlert("Dados Atualizados", "API Request ok!", "OK");
            }else
                DisplayAlert("Vishy", "Algo errado não está certo!", "OK");

        }

    }
}
