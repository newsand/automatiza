﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace XamanPort
{

    public class RestService
    {
        HttpClient client;
        private const string Url = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf={0}";

        public RestService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        

        public async Task<List<String>> GetResult()
        {
            Debug.WriteLine("Some text");
            try
            {
                var client = new HttpClient();
                var json = await client.GetStringAsync(string.Format(Url, ""));

                Debug.WriteLine("Some text");
                return JsonConvert.DeserializeObject<List<String>>(json.ToString());
            }
            catch (System.Exception exception)
            {
                return null;
            }

        }
    }
}
