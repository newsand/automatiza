﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace XamanPort
{
    class Pais
    {
        private string name;
        private string flag;
        private string capital;
        private string population;
        private string region;

        public Pais(string Name, string Flag, string Capital, string Population,string Region)
        {
            this.Name = Name;
            this.Flag = Flag;
            this.Capital = Capital;
            this.Population = Population;
            this.Region = Region;
        }

        public string Name { get => name; set => name = value; }
        public string Flag { get => flag; set => flag = value; }
        public string Capital { get => capital; set => capital = value; }
        public string Population { get => population; set => population = value; }
        public string Region { get => region; set => region = value; }

        public Dictionary<string, object> toDictionary()
        {
            Dictionary<string, object> v_parsedRegister = new Dictionary<string, object>();
            v_parsedRegister.Add("name", Name);
            v_parsedRegister.Add("flag", Flag);
            v_parsedRegister.Add("capital", Capital);
            v_parsedRegister.Add("population", Population);
            v_parsedRegister.Add("region", Region);
            return v_parsedRegister;
        }

        public static Pais fromJsonString(string p_jsonString)
        {
            Pais v_pais = JsonConvert.DeserializeObject<Pais>(p_jsonString);
            return v_pais;
        }
        
    }
}
