﻿using RestSharp.Portable.WebRequest;
using RestSharp.Portable;
using RestSharp.Portable.Deserializers;
using System;

namespace XamanPort
{
    class WebConnector
    {
        public string a;
        public Pais m_pais;
        private RestClient m_client;
        private string m_requestString;
        private JsonDeserializer m_jsonDeserializer;
        public WebConnector() { 
            m_client = new RestClient("https://restcountries.eu");
            m_requestString = "rest/v2/name/";
            m_jsonDeserializer = new JsonDeserializer();
        }

        public bool requestData(string p_countryName)
        {
            try
            {
                var v_request = m_requestString + p_countryName;
                var request = new RestRequest(v_request);
                var response = m_client.Execute(request);
                string newString = response.Result.Content.Substring(1, response.Result.Content.Length - 2);
                m_pais = Pais.fromJsonString(newString);
                return true;
            }
            catch (Exception e)
            { return false; }
           
        }
    }
}